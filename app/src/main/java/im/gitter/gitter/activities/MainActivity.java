package im.gitter.gitter.activities;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.LruCache;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import im.gitter.gitter.DrawerAdapter;
import im.gitter.gitter.LoginData;
import im.gitter.gitter.R;
import im.gitter.gitter.content.ContentProvider;
import im.gitter.gitter.content.RestServiceHelper;
import im.gitter.gitter.fragments.CommunityFragment;
import im.gitter.gitter.fragments.PeopleFragment;
import im.gitter.gitter.fragments.RoomFragment;
import im.gitter.gitter.fragments.AllConversationsFragment;
import im.gitter.gitter.models.Room;
import im.gitter.gitter.network.DeprecatedRoomRequest;
import im.gitter.gitter.notifications.RegistrationIntentService;

import static im.gitter.gitter.notifications.RegistrationIntentService.DEREGISTER_INTENT_KEY;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, DrawerLayout.DrawerListener {

    public static final String GO_TO_ROOM_ID_INTENT_KEY = "GO_TO_ROOM_ID";
    public static final String GO_TO_GROUP_ID_INTENT_KEY = "GO_TO_GROUP_ID";
    public static final String GO_TO_ALL_CONVERSATIONS_INTENT_KEY = "GO_TO_ALL_CONVERSATIONS";
    public static final String GO_TO_ALL_PEOPLE_INTENT_KEY = "GO_TO_PEOPLE";
    public static final String SIGN_OUT_INTENT_KEY = "SIGN_OUT";

    private static final int USER_LOADER = 0;
    private static final int UNREAD_ROOM_COUNT_LOADER = 1;
    private static final int USER_GROUPS_WITH_UNREAD_COUNTS_LOADER = 2;

    private static final int WEBVIEW_CACHE_SIZE = 5;

    private DrawerLayout drawerLayout;
    private RequestQueue requestQueue;
    private DrawerAdapter drawerAdapter;

    private final LruCache<String, WebView> webViewCache = new LruCache<>(WEBVIEW_CACHE_SIZE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // show a transparent status bar to allow for full screen navigation drawer on lollipop
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        actionBar.setDisplayHomeAsUpEnabled(true);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.addDrawerListener(this);

        LoaderManager loaderManager = getLoaderManager();
        loaderManager.initLoader(USER_LOADER, null, this);
        loaderManager.initLoader(UNREAD_ROOM_COUNT_LOADER, null, this);
        loaderManager.initLoader(USER_GROUPS_WITH_UNREAD_COUNTS_LOADER, null, this);

        drawerAdapter = new DrawerAdapter(this);
        RecyclerView drawer = (RecyclerView) findViewById(R.id.drawer_content);
        drawer.setLayoutManager(new LinearLayoutManager(this));
        drawer.setAdapter(drawerAdapter);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.content_container, new AllConversationsFragment())
                    .commit();
        }

        RestServiceHelper.getInstance().getUser(this, new LoginData(this).getUserId());

        requestQueue = Volley.newRequestQueue(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // cant handle intent here, as we could be paused
        setIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        Intent intent = getIntent();

        if (intent.hasExtra(GO_TO_ROOM_ID_INTENT_KEY)) {
            closeDrawer();
            navigateToRoom(intent.getStringExtra(GO_TO_ROOM_ID_INTENT_KEY));
        } else if (intent.hasExtra(GO_TO_ALL_CONVERSATIONS_INTENT_KEY)) {
            closeDrawer();
            navigateToAllConversations();
        } else if (intent.hasExtra(GO_TO_ALL_PEOPLE_INTENT_KEY)) {
            closeDrawer();
            navigateToPeopleList();
        } else if (intent.hasExtra(GO_TO_GROUP_ID_INTENT_KEY)) {
            closeDrawer();
            navigateToRoomListForGroup(intent.getStringExtra(GO_TO_GROUP_ID_INTENT_KEY));
        } else if (intent.hasExtra(SIGN_OUT_INTENT_KEY)) {
            signOut();
        }
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount() > 0) {
            Intent previousIntent = createIntentFromPreviousFragment();
            // fyi this doesn't pop the back stack immediately
            getFragmentManager().popBackStack();
            // set the correct intent so resuming from background doesn't show the fragment we just popped off
            startActivity(previousIntent);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        requestQueue.cancelAll(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        for (String key : webViewCache.snapshot().keySet()) {
            WebView webView = webViewCache.remove(key);
            webView.loadUrl("about:blank");
            webView.destroy();
        }
    }

    public LruCache<String, WebView> getWebViewCache() {
        return webViewCache;
    }

    public void openDrawer() {
        drawerLayout.openDrawer(GravityCompat.START);
        // update those unreads in the drawer
        RestServiceHelper.getInstance().getRoomList(this);
        RestServiceHelper.getInstance().getUserGroups(this);
    }

    private void closeDrawer() {
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    private void navigateToRoom(String roomId) {
        String name = "room:" + roomId;

        if (name.equals(getNameFromTopFragment())) {
            // room is already being shown
            return;
        }

        getFragmentManager().beginTransaction()
                .setCustomAnimations(
                        R.animator.slide_left_enter, R.animator.slide_left_exit,
                        R.animator.slide_right_enter, R.animator.slide_right_exit
                )
                .replace(R.id.content_container, RoomFragment.newInstance(roomId))
                .addToBackStack(name)
                .commit();
    }

    private void navigateToRoomListForGroup(String groupId) {
        String name = "group:" + groupId;

        if (name.equals(getNameFromTopFragment())) {
            // group is already being shown
            return;
        }

        // dont worry, the navigation animations will merge
        navigateToAllConversations();
        // put the group on top of the All Conversations fragment
        getFragmentManager().beginTransaction()
                .replace(R.id.content_container, CommunityFragment.newInstance(groupId))
                .addToBackStack(name)
                .commit();
    }

    private void navigateToAllConversations() {
        // empty out the back stack to get the All Conversations fragment
        // noops if we're already there
        getFragmentManager().popBackStack(null, 0);
    }

    private void navigateToPeopleList() {
        String name = "people";

        if (name.equals(getNameFromTopFragment())) {
            // people list is already being shown
            return;
        }

        // dont worry, the navigation animations will merge
        navigateToAllConversations();
        // put the people list on top of the All Conversations fragment
        getFragmentManager().beginTransaction()
                .replace(R.id.content_container, new PeopleFragment())
                .addToBackStack(name)
                .commit();
    }

    public void createOrJoinRoom(String uri) {
        try {
            requestQueue.add(new DeprecatedRoomRequest(
                    this,
                    Request.Method.POST,
                    "/v1/rooms",
                    new JSONObject("{ uri: '" + uri + "'}"),
                    new Response.Listener<Room>() {
                        @Override
                        public void onResponse(Room response) {
                            navigateToRoom(response.getId());
                        }
                    },
                    createGetRoomFailureListener()
            ).setTag(this));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Response.ErrorListener createGetRoomFailureListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(
                        MainActivity.this,
                        R.string.network_failed,
                        Toast.LENGTH_SHORT
                ).show();
            }
        };
    }

    private void signOut() {
        new LoginData(this).clear();

        Intent intent = new Intent(this, RegistrationIntentService.class);
        intent.putExtra(DEREGISTER_INTENT_KEY, true);
        startService(intent);

        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        ContentResolver contentResolver = getContentResolver();
        contentResolver.delete(ContentProvider.ADMIN_GROUPS_CONTENT_URI, null, null);
        contentResolver.delete(ContentProvider.USER_GROUPS_CONTENT_URI, null, null);
        contentResolver.delete(ContentProvider.USER_ROOMS_CONTENT_URI, null, null);
        contentResolver.delete(ContentProvider.ROOMS_CONTENT_URI, null, null);
        contentResolver.delete(ContentProvider.GROUPS_CONTENT_URI, null, null);
        contentResolver.delete(ContentProvider.USERS_CONTENT_URI, null, null);

        startActivity(new Intent(this, StartActivity.class));

        // stop users from navigating back to this activity
        finish();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case USER_LOADER:
                return new CursorLoader(this, Uri.withAppendedPath(ContentProvider.USERS_CONTENT_URI, new LoginData(this).getUserId()), null, null, null, null);
            case UNREAD_ROOM_COUNT_LOADER:
                return new CursorLoader(this,
                        ContentProvider.ROOMS_CONTENT_URI,
                        new String[]{
                                "sum(case when unreadItems > 0 then 1 else 0 end) unreadRooms",
                                "sum(case when mentions > 0 then 1 else 0 end) mentionedRooms",
                                "sum(case when unreadItems > 0 AND oneToOne then 1 else 0 end) unreadOneToOnes",
                                "sum(case when mentions > 0 AND oneToOne then 1 else 0 end) mentionedOneToOnes"
                        },
                        null,
                        null,
                        null);
            case USER_GROUPS_WITH_UNREAD_COUNTS_LOADER:
                return new CursorLoader(this, ContentProvider.USER_GROUPS_WITH_UNREADS_CONTENT_URI, null, null, null, null);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        switch (loader.getId()) {
            case USER_LOADER:
                drawerAdapter.setUser(cursor);
                break;
            case UNREAD_ROOM_COUNT_LOADER:
                drawerAdapter.setUnreadCounts(cursor);
                break;
            case USER_GROUPS_WITH_UNREAD_COUNTS_LOADER:
                drawerAdapter.setUserGroupsWithUnreadCounts(cursor);
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case USER_LOADER:
                drawerAdapter.setUser(null);
                break;
            case UNREAD_ROOM_COUNT_LOADER:
                drawerAdapter.setUnreadCounts(null);
                break;
            case USER_GROUPS_WITH_UNREAD_COUNTS_LOADER:
                drawerAdapter.setUserGroupsWithUnreadCounts(null);
                break;
        }
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {}

    @Override
    public void onDrawerOpened(View drawerView) {}

    @Override
    public void onDrawerClosed(View drawerView) {}

    @Override
    public void onDrawerStateChanged(int newState) {
        if (newState == DrawerLayout.STATE_DRAGGING && !drawerLayout.isDrawerOpen(GravityCompat.START)) {
            // user is dragging the drawer open, update those unreads in the drawer
            RestServiceHelper.getInstance().getRoomList(this);
            RestServiceHelper.getInstance().getUserGroups(this);
        }
    }

    private Intent createIntentFromPreviousFragment() {
        Intent intent = new Intent(this, MainActivity.class);

        FragmentManager manager = getFragmentManager();
        int backStackCount = manager.getBackStackEntryCount();

        if (backStackCount < 2) {
            intent.putExtra(GO_TO_ALL_CONVERSATIONS_INTENT_KEY, true);
        } else {
            String name = manager.getBackStackEntryAt(backStackCount - 2).getName();
            if ("people".equals(name)) {
                intent.putExtra(GO_TO_ALL_PEOPLE_INTENT_KEY, true);
            } else if (name.startsWith("room:")) {
                String roomId = name.split(":")[1];
                intent.putExtra(GO_TO_ROOM_ID_INTENT_KEY, roomId);
            } else if (name.startsWith("group:")) {
                String groupId = name.split(":")[1];
                intent.putExtra(GO_TO_GROUP_ID_INTENT_KEY, groupId);
            } else {
                throw new IllegalArgumentException("Cannot understand back stack entry with name: " + name);
            }
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return intent;
    }

    private @Nullable String getNameFromTopFragment() {
        FragmentManager manager = getFragmentManager();
        int backStackCount = manager.getBackStackEntryCount();
        if (backStackCount < 1) {
            return null;
        } else {
            return manager.getBackStackEntryAt(backStackCount - 1).getName();
        }
    }
}
